Phab Ban
========

Disable Phabricator user accounts after verifying that the requesting user is
a member of a privileged project.

License
-------
[GNU GPLv3+](//www.gnu.org/copyleft/gpl.html "GNU GPLv3+")

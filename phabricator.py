# -*- coding: utf-8 -*-
#
# This file is part of Phab Ban
#
# Copyright (C) 2018 Bryan Davis and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Phabricator client."""
import json
import logging

import requests


logger = logging.getLogger(__name__)


class APIError(Exception):
    """Errors from Phabricator."""

    def __init__(self, message, code, result):
        """Constructor."""
        self.message = message
        self.code = code
        self.result = result

    def __str__(self):
        """To String."""
        return "{0} ({1})".format(self.message, self.code)


class Client(object):
    """Phabricator client."""

    def __init__(self, url, username, token):
        """New client."""
        self.url = url
        self.username = username
        self.session = {
            "token": token,
        }

    def post(self, path, data):
        """POST request."""
        data["__conduit__"] = self.session
        r = requests.post(
            "{0}/api/{1}".format(self.url, path),
            data={"params": json.dumps(data), "output": "json"},
        )
        resp = r.json()
        logger.debug("%s result: %s", path, resp)
        if resp["error_code"] is not None:
            raise APIError(
                resp["error_info"],
                resp["error_code"],
                resp.get("result", None),
            )
        return resp["result"]

    def user_search(self, username):
        """Lookup Phabricator user data."""
        r = self.post(
            "user.search",
            {"constraints": {"usernames": [username]}, "limit": 1},
        )
        return r["data"][0]

    def user_disable(self, phid):
        """Disable the given user."""
        r = self.post(
            "user.edit",
            {
                "transactions": [{"type": "disabled", "value": True}],
                "objectIdentifier": phid,
            },
        )
        return r

    def project_members(self, name):
        """Lookup a list of PHIDs that are members of a given project."""
        r = self.post(
            "project.search",
            {
                "constraints": {"name": name},
                "attachments": {"members": True},
                "limit": 1,
            },
        )
        members = r["data"][0]["attachments"]["members"]["members"]
        return [m["phid"] for m in members]
